/* EnableInterrupts宏定义 */
#include <hidef.h>

/* 内存函数定义 */
#include <string.h>

/* 外设定义 */
#include "derivative.h"

/* 禁止: Warning: C2705: Possible loss of data */
#pragma MESSAGE DISABLE C2705 

/* SPI缓存大小 */           
#define SPI_READ_BUFFER_SIZE (3)
#define SPI_WRITE_BUFFER_SIZE (3)

#if SPI_READ_BUFFER_SIZE != 3
#error SPI_READ_BUFFER_SIZE must be 3    
#endif
#if SPI_WRITE_BUFFER_SIZE != 3
#error SPI_WRITE_BUFFER_SIZE must be 3
#endif

/* SCI1缓存大小 */
#define SCI1_RX_BUFFER_SIZE (255)
#define SCI1_TX_BUFFER_SIZE (255)

#if SCI1_RX_BUFFER_SIZE != 255 
#error SCI1_RX_BUFFER_SIZE must be 255
#endif
#if SCI1_TX_BUFFER_SIZE != 255 
#error SCI1_TX_BUFFER_SIZE must be 255
#endif

/* SCI1采集超时次数 */
#define SCI1_GATHER_TIMEOUT (3)

#if SCI1_GATHER_TIMEOUT != 3
#error SCI1_GATHER_TIMEOUT must be 3
#endif

/* SPI读缓存 */
uchar SPI_Read_Ptr = 0;
uchar SPI_Read_Buffer[SPI_READ_BUFFER_SIZE] = {0};
/* SPI写缓存 */
uchar SPI_Write_Ptr = 0;
uchar SPI_Write_Buffer[SPI_WRITE_BUFFER_SIZE] = {0};

/* SCI1接收缓存 */
uchar SCI1_Rx_Ptr = 0;
uchar SCI1_Rx_Buffer[SCI1_RX_BUFFER_SIZE] = {0};
/* SCI1发送缓存 */
uchar SCI1_Tx_Len = 0;
uchar SCI1_Tx_Ptr = 0;
uchar SCI1_Tx_Buffer[SCI1_TX_BUFFER_SIZE] = {0};    

/* Modem IRQ标志 */
uchar Modem_IRQ = FALSE;

/* IRQ状态寄存器 */
uint IRQ_Status = 0;

/* SCI1采集超时 */
uchar SCI1_Gather_Timeout = 0;

/* IRQ ISR */
void interrupt VectorNumber_Virq irq_isr() {
  IRQSC_IRQACK = TRUE;
  
  Modem_IRQ = TRUE;
}

/* SPI ISR */
void interrupt VectorNumber_Vspi1 spi_irq() {
  /* 读 */
  if (SPI1S_SPRF) { 
    SPI_Read_Buffer[SPI_Read_Ptr] = SPI1D;
    SPI_Read_Ptr = (SPI_Read_Ptr + 1) % SPI_READ_BUFFER_SIZE; 
    
    /* 已读完, 取消片选 */
    if (0 == SPI_Read_Ptr) {
      PTED_PTED2 = TRUE;
    }
  }
  
  /* 写 */
  if (SPI1S_SPTEF) {
    if (SPI_Write_Ptr < SPI_WRITE_BUFFER_SIZE) {   
      SPI1D = SPI_Write_Buffer[SPI_Write_Ptr++]; 
    } else { 
      /* 已写完, 禁止中断 */
      SPI1C1_SPTIE = FALSE;  
    }
  } 
} 

/* SCI1 Rx ISR */      
void interrupt VectorNumber_Vsci1rx sci1_rx_isr() {
  if (SCI1S1_RDRF) {
    SCI1_Rx_Buffer[SCI1_Rx_Ptr] = SCI1D;
    SCI1_Rx_Ptr = (SCI1_Rx_Ptr + 1) % SCI1_RX_BUFFER_SIZE;
    
    /* 开启RTI超时 */
    SCI1_Gather_Timeout = 1;
  }
}

/* SCI1 Tx ISR */
void interrupt VectorNumber_Vsci1tx sci1_tx_isr() {
  if (SCI1S1_TDRE) {
    if (SCI1_Tx_Ptr < SCI1_Tx_Len) {
      SCI1D = SCI1_Tx_Buffer[SCI1_Tx_Ptr++];
    } else { 
      /* 已写完, 禁止中断 */
      SCI1C2_TIE = FALSE;
    }
  }
}

/* RTI ISR */
void interrupt VectorNumber_Vrti rti_isr() {
  SRTISC_RTIACK = TRUE;
  
  /* SCI1抓取计时 */
  if (SCI1_Gather_Timeout) {
    if (++SCI1_Gather_Timeout > SCI1_GATHER_TIMEOUT) {  
      SCI1C2_RE = FALSE;
      SCI1_Gather_Timeout = 0;
    }
  }
}

/* 读Modem寄存器 */
uint modem_read(uchar reg) {
  /* 填充SPI写缓存 */
  SPI_Write_Ptr = 0;
  SPI_Write_Buffer[0] = (1 << 7) | reg;
  SPI_Write_Buffer[1] = 0;
  SPI_Write_Buffer[2] = 0;
  
  /* 选中Modem SPI */
  PTED_PTED2 = FALSE;
  
  /* 开始写 */
  SPI1C1_SPTIE = TRUE;
  
  /* 等待读完成 */
  while (FALSE == PTED_PTED2) {}
  
  return ((uint)SPI_Read_Buffer[1] << 8) | SPI_Read_Buffer[2];
}

/* 写Modem寄存器 */
void modem_write(uchar reg, uint value) {
  /* 填充SPI写缓存 */
  SPI_Write_Ptr = 0;
  SPI_Write_Buffer[0] = reg;
  SPI_Write_Buffer[1] = (value >> 8) & 0xFF;
  SPI_Write_Buffer[2] = value & 0xFF;
  
  /* 选中Modem SPI */
  PTED_PTED2 = FALSE;
  
  /* 开始写 */
  SPI1C1_SPTIE = TRUE;
  
  /* 等待写完成 */
  while (FALSE == PTED_PTED2) {}
}

/* SCI1读 */
uchar sci1_read(uchar *dst) {
  uchar len;
  
  /* 数据长度 */
  len = SCI1_Rx_Ptr;
  
  /* 读数据 */
  (void)memcpy(dst, SCI1_Rx_Buffer, len);
  
  /* 复位接收缓存 */
  SCI1_Rx_Ptr = 0;
  
  return len;
}

/* SCI1写 */
void sci1_write(uchar *src, uchar len) {
  /* 等待写完成 */
  while (TRUE == SCI1C2_TIE) {}

  /* 数据长度 */
  SCI1_Tx_Len = len;
  
  /* 写数据 */
  (void)memcpy(SCI1_Tx_Buffer, src, len);
  
  /* 复位发送缓存 */
  SCI1_Tx_Ptr = 0;
  
  /* 开始发送 */
  SCI1C2_TIE = TRUE;
}

/* 内核初始化 */
void core_init() {
  /* 禁止COP */
  //SOPT_COPE = FALSE;
  
  /* 使能全局中断 */
  EnableInterrupts;
  
  /* 使能IRQ */                  
  IRQSC_IRQIE = TRUE;  // 接收Modem的中断请求
  IRQSC_IRQPE = TRUE;  // 使能IRQ后, MCU自动上拉
  
  /* 设置ATTN脚 */ 
  PTDDD_PTDDD0 = TRUE; // PTD0: 将Modem从Hibernate或Doze模式唤醒
  PTDD_PTDD0 = TRUE;   // ATTN为低电有效
  
  /* 设置M_RST脚 */                
  PTDDD_PTDDD3 = TRUE; // PTD3: 复位Modem
  PTDD_PTDD3 = TRUE;   // M_RST为低电有效
  
  /* 使能SPI */
  PTEDD_PTEDD2 = TRUE;   // PTE2: SPI SS, 必须作为GPIO来控制
  PTED_PTED2 = TRUE;     // SPI SS为低电有效     
   
  SPI1C1_MSTR = TRUE;    // MCU为主
  SPI1C1_CPHA = FALSE;   // CLK相位为0
  SPI1C1_SPIE = TRUE;    // 开启接收中断
  SPI1C1_SPE = TRUE;     // 使能SPI  
  
  /* 等待Modem复位完成 */
  while (FALSE == Modem_IRQ) {} 
  
  /* 检查 Modem Chip_ID */
  {
    uint chip_id = modem_read(0x2C);
    
    if (0x6800 == chip_id) {
      modem_write(0x31, 0xA0C0);
      modem_write(0x34, 0xFEC6);
    }
  }
  
  /* 增强Modem驱动强度, 以提高性能 */
  {
    uint gpio_data_out;
    
    gpio_data_out = modem_read(0x0C);
    gpio_data_out |= 0x0C00; // 设置CLKO为最大驱动强度
    modem_write(0x0C, gpio_data_out);
  }
  
  /* 重设Modem CLKO为2MHz */
  {
    uint clko_ctl; 
    
    clko_ctl = modem_read(0x0A);
    clko_ctl = clko_ctl & 0xFFF8 | 0x03; // 2MHz
    modem_write(0x0A, clko_ctl);
  }
  
  /* 切换MCU时钟为8MHz */
  ICGC2_MFD = 0x03;   // 10倍频
  ICGC2_LOLRE = TRUE; // 如果外部时钟丢失
  ICGC2_LOCRE = TRUE; // 则发出复位请求
  ICGC1_CLKS = 0x03;  // 设置时钟为FEE模式
  
  /* 等待时钟切换完成, SPI CLK为5MHz */
  while (0x03 != ICGS1_CLKST) {} 
  
  /* 使能RXTXEN脚 */   
  PTDDD_PTDDD1 = TRUE; // PTD1: 使能Modem的RX, TX或CCA操作
                       // RXTXEN为高电有效
  
  /* 使能Modem GPIO1脚 */
  PTEPE_PTEPE6 = TRUE; // PTE6: Modem GPIO1/Out_Of_Idle
                       // PTE6必须上拉, 因为Modem端没有上拉
  
  /* 使能Modem GPIO2脚 */
  PTEPE_PTEPE7 = TRUE; // PTE7: Modem GPIO2/CRC_Valid
                       // PTE7必须上拉, 因为Modem端没有上拉
                       
  /* 使能RTI */
  SRTISC_RTICLKS = TRUE; // RUN模式时, 必须引用外部时钟 
  SRTISC_RTIS = 0x03;    // 频率大约为1ms触发一次中断
  SRTISC_RTIE = TRUE;    // 开启中断
  
  /* 使能SCI1: BUSCLK为10MHz时, 125000 8 None 1 */
  SCI1BDH = 0; // 其波特率为125000
  SCI1BDL = 5;
  SCI1C2_RIE = TRUE; // 开启接收中断
  SCI1C2_TE = TRUE;  // 开启发送器
  SCI1C2_RE = TRUE;  // 开启接收器
  
  /* 设置Modem为Stream模式 */ 
  {
    //uint original;
    
    /* 设置通过SPI进行实时数据传输 */
    //original = modem_reg_read(0x06);
    //original |= 0x1800;
    //modem_reg_write(0x06, original);
    
    /* 设置Modem工作在Stream模式 */
    //original = modem_reg_read(0x07);
    //original |= 0x0020;
    //modem_reg_write(0x07, original);
  }
  
  /* 初始化D1: PTC5 */
  PTCDD_PTCDD5 = TRUE; // 输出
}

void main(void) {
  uint i = 0;

  core_init();

  for(;;) {
    /* 复位COP */
    __RESET_WATCHDOG();
  
    /* Modem IRQ */
    if (Modem_IRQ) {
      Modem_IRQ = FALSE;
    
      /* 读取IRQ_Status */ 
      IRQ_Status = modem_read(0x24); 
    }
    
    /* IRQ状态 */
    if (IRQ_Status) {
      /* crc_valid */
      if (IRQ_Status & ((uint)0x01 << 0)) {
      }
      
      /* cca */
      if (IRQ_Status & ((uint)0x01 << 1)) {
      }  
      
      /* tmr2_irq */
      if (IRQ_Status & ((uint)0x01 << 2)) {
      }
      
      /* tmr4_irq */
      if (IRQ_Status & ((uint)0x01 << 3)) {
      }
      
      /* tmr3_irq */
      if (IRQ_Status & ((uint)0x01 << 4)) {
      }
      
      /* cca_irq */
      if (IRQ_Status & ((uint)0x01 << 5)) {
      }
      
      /* tx_sent_irq or tx_strm_irq */
      if (IRQ_Status & ((uint)0x01 << 6)) {
      }
      
      /* rx_rcvd_irq or rx_strm_irq */
      if (IRQ_Status & ((uint)0x01 << 7)) {
      }
      
      /* tmr1_irq */
      if (IRQ_Status & ((uint)0x01 << 8)) {
      }
      
      /* doze_irq */
      if (IRQ_Status & ((uint)0x01 << 9)) {
      }
      
      /* attn_irq */
      if (IRQ_Status & ((uint)0x01 << 10)) {
      }
      
      /* strm_data_err */
      if (IRQ_Status & ((uint)0x01 << 12)) {
      }
      
      /* arb_busy_err or rx_done_irq */
      if (IRQ_Status & ((uint)0x01 << 13)) {
      }
      
      /* ram_addr_err or tx_done_irq */
      if (IRQ_Status & ((uint)0x01 << 14)) {
      } 
      
      /* pll_lock_irq */
      if (IRQ_Status & ((uint)0x01 << 15)) {
      } 
      
      IRQ_Status = 0;
    }
    
    /* 从SCI1抓取一条数据 */
    if (FALSE == SCI1C2_RE) {
      uchar len;
      uchar buffer[SCI1_RX_BUFFER_SIZE];
      
      len = sci1_read(buffer);
    
      SCI1C2_RE = TRUE;
      
      sci1_write(buffer, len);
    }
    
    /* 翻转D1: PTC5 */
    i = (i + 1) % 1000;
    if (0 == i) {
      PTCD_PTCD5 = !PTCD_PTCD5;
    }
  }
}
