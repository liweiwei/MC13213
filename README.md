# 简介
[**MC13213**](https://www.nxp.com/pages/2.4-ghz-802.15.4-rf-and-8-bit-hcs08-mcu-with-60kb-flash-4kb-ram:MC13213)是飞思卡尔的一款低功耗, 集成ZigBee, 802.15.4标准和HS08内核的片上系统.
# 项目
该项目用于研究MC13213开发流程和功能验证.  
使用ZSTAR3作为开发板, CodeWarrior作为IDE.  
STDCC开发环境搭建见: https://gitlab.com/LiWeiwei/ZSTAR3.
# 总结
MC13213内建了多个工作模式, Modem支持Packet和Stream模式. 可提供优秀的低功耗性能.  
同时, Modem还具有时钟输出, GPIO, Timer等扩展功能, 可以很好的弥补HCS08的外设资源不足.  
# 缺点
MC13213已停产, HCS08内核很老, 目前市面上Arm Cortex M0的内核更为通用.  
故, MC13213不应选作新设计.
